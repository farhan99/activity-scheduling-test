<?php 
$servername = "localhost";
$username   = "root";
$password   = "";
$dbname 	= "das_account";
$conn   = new mysqli($servername, $username, $password, $dbname);

/*Check Connection*/
if($conn->connect_error){
	die("Connection Failed: " . $conn->connect_error);
}
$callback = $_REQUEST['callback'];
$login_user_id = $_REQUEST['user_id'];

//Create the output object
$output  = array();
$success = 'false';

//Selection
if ($login_user_id == -1) {
	$query = "select * from profile" or die("Cannot access item");
} else {
	$query = "select * from profile where user_id = $login_user_id" or die("Cannot access item");
}
$result  = mysqli_query($conn, $query);
if(mysqli_num_rows($result) > 0){
	while ($obj = mysqli_fetch_object($result)) {
		$output[] = $obj;
	}
	$success = 'true';
}

//Start Output
if($callback){
	header('Content-Type: text/javascript');
	echo $callback . '({"Success":' . $success . ', "items": '. json_encode($output) . '});';
} else {
	header('Content-Type: application/x-json');
	echo json_encode($output);
}
$conn->close();
?>