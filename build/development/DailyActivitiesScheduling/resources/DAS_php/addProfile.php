<?php
$servername = "localhost";
$username   = "root";
$password   = "";
$dbname     = "das_account";
$conn = new mysqli($servername, $username,  $password, $dbname);
/* check connection */
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$callback = $_REQUEST['callback'];
$records  = json_decode($_REQUEST['records']);
//$user_id  = $records->{"user_id"};
$name     = $records->{'nama_lengkap'};
$no_id    = $records->{'no_identitas'};
$email    = $records->{'user_email'};
$phone    = $records->{'no_telp'};
$cover    = $records->{'gambar'};
$website  = $records->{'user_web'};
$bio  	  = $records->{'biodata'};
// Create the output object.
$output  = array();
$success = 'false';
$query   = "insert into profile 
			(nama_lengkap, no_identitas, user_email, no_telp, gambar, user_web, biodata) 
			values ('$name', '$no_id', '$email', '$phone', '$cover', '$website', '$bio')";

if ($conn->query($query) === TRUE) {
	$success = 'true';
} else{
	$success = 'false';
	$error = $conn->error;
}
//start output
if ($callback) {
	header('Content-Type: text/javascript');
	echo $callback . '({"success":'.$success.', "items":' .
	json_encode($output) . '});';
} else {
 	header('Content-Type: application/x-json');
 	echo json_encode($output);
}
$conn->close();
?>