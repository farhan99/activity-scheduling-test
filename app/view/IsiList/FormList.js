Ext.define('DailyActivitiesScheduling.view.IsiList.FormList', {
    extend: 'Ext.form.Panel',
    extend: 'Ext.grid.Grid',
    //extend: 'Ext.tab.Panel',
    xtype: 'formlist',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],

    title: 'FORM PENGISIAN AKTIFITAS',
    shadow: true,
    cls: 'demo-solid-background',
    //id: 'basicform',
    defaults: {
        scrollable: true
		//margin: '0 0 10 0',
        //bodyPadding: 10,
	},
    
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'ISI FORM DI BAWAH',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {

                    xtype: 'selectfield',
                    name: 'rank',
                    label: 'Pilih Hari ;',
                    options: [
                        {
                            text: 'Senin',
                            value: 'senin'
                        },
                        {
                            text: 'Selasa',
                            value: 'selasa'
                        },
                        {
                            text: 'Rabu',
                            value: 'rabu'
                        },
                        {
                            text: 'Kamis',
                            value: 'kamis'
                        },
                        {
                            text: 'Jumat',
                            value: 'jumat'
                        },
                        {
                            text: 'Sabtu',
                            value: 'sabtu'
                        },
                        {
                            text: 'Minggu',
                            value: 'minggu'
                        }
                    ]
                },
                {
                    label: 'Jenis Aktivitas'
                },
                {
                    label: 'Jenis Aktivitas',
                    title: 'Jenis Aktivitas',
                    xtype: 'checkboxfield',
                    name: 'pekerjaan',
                    label: '<b>Pekerjaan</b>',
                    platformConfig: {
                        '!desktop': {
                            bodyAlign: 'end'
                        }
                    }
                },
                {
                    xtype: 'checkboxfield',
                    name: 'liburan',
                    label: '<b>Liburan</b>',
                    platformConfig: {
                        '!desktop': {
                            bodyAlign: 'end'
                        }
                    }
                },
                {
                    xtype: 'checkboxfield',
                    name: 'tugas',
                    label: '<b>Tugas</b>',
                    platformConfig: {
                        '!desktop': {
                            bodyAlign: 'end'
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Aktivitas :',
                    placeHolder: 'Task',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Set Waktu:',
                    placeHolder: '23.59',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                //-----------------------TEXT
                {
                    xtype: 'textareafield',
                    name: 'bio',
                    label: 'Keterangan'
                },

                //--------------------------------------------------------------------
                //--------------------------------------------------------------------
                //-------------------------------BUTTON-------------------------------------
                {
                    xtype: 'container',
                    defaults: {
                        xtype: 'button',
                        style: 'margin: 1em',
                        flex: 1
                    },
                    layout: {
                        type: 'hbox'
                    },

                    items: [
                        {
                            text: 'SIMPAN',
                            ui: 'action',
                            handler: function () {
                                Ext.Msg.alert('Alert', 'Data Tersimpan, pindah ke Home');
                                var mainView = Ext.getCmp('app-main');
                                mainView.setActiveItem(0);
                            }
                        },
                        {
                            text: 'Reset',
                            ui: 'action',
                            handler: function () {
                                Ext.getCmp('basicform').reset();
                            }
                        },

                    ]
                }



            ]
        }]
});