Ext.define('DailyActivitiesScheduling.view.login.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'login',
    controller: 'login',
    title: '<center>Login User</center>',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password',
        'DailyActivitiesScheduling.view.login.LoginController'
    ],

    shadow: true,
    cls: 'demo-solid-background',
    id: 'formlogin',
    items: [
        {
            xtype: 'fieldset',
            id: 'user-login',
            title: 'Silahkan Login',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    html: '<center><img src="../../resources/user-login-img.png" style="max-width: 200px"></center>'
                },
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Username',
                    placeHolder: 'Masukkan nama pengguna',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    placeHolder: 'Masukkan kata sandi',
                    clearIcon: true
                },
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Login',
                    ui: 'action',
                    handler: 'onLogin'
                }
            ]
        }
    ]
});