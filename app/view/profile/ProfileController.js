/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('DailyActivitiesScheduling.view.profile.ProfileController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.profile',

    onItemSelected: function (sender, record) {
        Ext.getStore('individuals').getProxy().setExtraParams({
            user_id: record.data.user_id
        }); 
        Ext.getStore('individuals').load();
    },

    /*onReadClicked: function(){
        Ext.getStore('individuals').load();
    },*/

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onSimpanPerubahan: function(){

        name  = Ext.getCmp('myName').getValue();
        npm   = Ext.getCmp('myNPM').getValue();
        email = Ext.getCmp('myEmail').getValue();
        phone = Ext.getCmp('myPhone').getValue();
        cover = Ext.getCmp('myCover').getValue();
        site  = Ext.getCmp('mySite').getValue();
        bio   = Ext.getCmp('myBio').getValue();

        store  = Ext.getStore('individuals');
        record = Ext.getCmp('mydataview').getSelection();
        index  = store.indexOf(record);
        record = store.getAt(index);

        store.beginUpdate();
        record.set('nama_lengkap', name);
        record.set('no_identitas', npm);
        record.set('user_email', email);
        record.set('no_telp', phone);
        record.set('gambar', cover);
        record.set('user_web', site);
        record.set('biodata', bio);
        store.endUpdate();
        alert("Saving... / Menyimpan...");
        Ext.getCmp('data-profile').setActiveItem(0);
    },

    onTambahData: function() {
        name  = Ext.getCmp('myName').getValue();
        npm   = Ext.getCmp('myNPM').getValue();
        email = Ext.getCmp('myEmail').getValue();
        phone = Ext.getCmp('myPhone').getValue();
        cover = Ext.getCmp('myCover').getValue();
        site  = Ext.getCmp('mySite').getValue();
        bio   = Ext.getCmp('myBio').getValue();
        
        store = Ext.getStore('individuals');
        store.beginUpdate();
        store.insert(0, {
            'nama_lengkap': name, 'no_identitas': npm, 'user_email': email,
            'no_telp': phone, 'gambar': cover, 'user_web': site,
            'biodata': bio
        })
        store.endUpdate();
        alert("Inserting...");
        Ext.getCmp('data-profile').setActiveItem(0);
    }
});

function onDeleteProfile(user_id){
    record = Ext.getCmp('mydataview').getSelection();
    //Ext.Msg.confirm('Delete / Hapus Data', '<center>Konfirmasi Penghapusan Data?</center>', Ext.emptyFn);
    Ext.getStore('individuals').remove(record);
    //alert(user_id);
}

function onUpdateProfile(user_id){
    //store  = Ext.getStore('individuals');
    record = Ext.getCmp('mydataview').getSelection();
    //formView = Ext.getCmp('basic-info').setActiveItem(1);
    //formView = Ext.getCmp('pengembang').setActiveItem(1);
    // //formView = Ext.getCmp('app-pengembang').setActiveItem(2);
    formView = Ext.getCmp('data-profile').setActiveItem(1);
    name  = record.data.nama_lengkap;
    npm   = record.data.no_identitas;
    email = record.data.user_email; 
    phone = record.data.no_telp;
    site  = record.data.user_web;
    cover = record.data.gambar;
    bio   = record.data.biodata;
    Ext.getCmp('myName').setValue(name);
    Ext.getCmp('myNPM').setValue(npm);
    Ext.getCmp('myEmail').setValue(email);
    Ext.getCmp('myPhone').setValue(phone);
    Ext.getCmp('mySite').setValue(site);
    Ext.getCmp('myCover').setValue(cover);
    Ext.getCmp('myBio').setValue(bio);
    /*record = store.getAt(index);
    store.beginUpdate();
    record.set('nama_lengkap', "Farhan Minase");
    store.endUpdate();
    alert("Updating...");*/
}
