Ext.define('DailyActivitiesScheduling.view.profile.Pengembang', {
    extend: 'Ext.Container',
    extend: 'Ext.grid.Grid',
    extend: 'Ext.Panel',
    xtype: 'pengembang',
	id: 'app-pengembang',
    
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Ext.Menu',
        'Ext.field.Search'
    ],
    
    viewModel: {
        stores: {
            Individuals: {
                type: 'individuals'
            }
        }
    },
    
    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [   
    {  //Toolbar
        docked: 'top',
        xtype: 'toolbar',
        scrollable: 'horizontal',
        items: [
        /*{
            xtype: 'button',
            ui: 'action',
            text: 'Refresh',
            scope: this,
            listeners: {
                tap: 'onReadClicked'
            }
        },*/
            {
                xtype: 'searchfield',
                placeHolder: 'Search by Name',
                name: 'searchfield',
                width: '100%',
                listeners: {
                    change: function(me, newValue, oldValue,eOpts){ 
                        indivStore = Ext.getStore('individuals');
                        indivStore.filter('nama_lengkap', newValue);
                    }
                }
            }, {xtype: 'spacer', width: '10%'},
            {
                xtype: 'searchfield',
                placeHolder: 'Search by NPM',
                name: 'searchfield',
                width: '100%',
                listeners: {
                    change: function(me, newValue, oldValue,eOpts){ 
                        indivStore = Ext.getStore('individuals');
                        indivStore.filter('no_identitas', newValue);
                    }
                }
            }, {xtype: 'spacer', width: '10%'},
            {
                xtype: 'searchfield',
                placeHolder: 'Search by Email',
                name: 'searchfield',
                width: '100%',
                listeners: {
                    change: function(me, newValue, oldValue,eOpts){ 
                        indivStore = Ext.getStore('individuals');
                        indivStore.filter('user_email', newValue);
                    }
                }
            }, //{xtype: 'spacer', width: '15%'}
        ]
    }, 
    {
        xtype: 'dataview',
        id: 'mydataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl:

            '<div class="container my-2 px-0">'+
                '<div class="card border-secondary mx-3">'+
                    '<div class="row no-gutters">'+
                        '<div class="col">'+
                            '<img src="{gambar}" class="card-img" alt="...">'+
                            '<h1 class="card-title">{nama_lengkap}</h1>'+ //NAMA
                            '<p class="card-sub-title"><b>{no_identitas}</b></p>'+ //NPM
                            '<p class="card-text"><i>Phone: </i>{no_telp}</p>'+ //PHONE
                            '<p class="card-text"><i>Email: </i>{user_email}</p>'+ //EMAIL
                            /*'<div class="card-footer py-2 text-muted" style="text-align: center";><a href="{user_web}" class="btn btn-primary">Website</a><button class="btn btn-danger">Edit</button></div>'+*/
                        '</div>'+
                        //'<div class="col-md-8">'+
                            //'<div class="card-body">'+
                                // '<center><a href="{user_web}" class="btn btn-primary">Website</a></div></div></center>'+
                                // '<p class="card-text" style="text-align: left; margin: 0 0 15px 15px" ><small class="text-muted"><br>Last updated 3 mins ago</small></p>'+
                            //'</div>'+
                            ///'<div class="card-footer text-muted" style="text-align: center";><a href="{user_web}" class="btn btn-primary">Website</a></div>'+
                        //'</div>'+
                    '</div>'+
                    '<div class="row">' +
                        '<div class="col" align=center>' +
                            '<div class="card-footer py-2 text-muted">'+
                                '<a href="{user_web}" class="btn btn-primary mx-1">Website</a>'+
                                '<button class="btn btn-outline-warning mx-1" type="button" onclick="onUpdateProfile({user_id})">Edit</button>' +
                                '<button class="btn btn-outline-danger mx-1" type="button" onclick="onDeleteProfile({user_id})">Delete</button>'+ 
                            '</div>' +            
                        '</div>'+
                    '</div>' +
                '</div>'+
            '</div>',
        /*itemTpl:
            '<img src="{gambar}" class="user-img" alt=""><h2>{nama_lengkap}</h2><hr>',*/

        bind: {
            store: '{Individuals}',
        },
        /*plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?',
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.card-img',
            allowOver: true,
            anchor: true,
            //trackMouse: true,
            
            bind: '{record}',
            tpl: 
                '<table class="hoverTable">' + 
                    '<tr>' +
                        '<td width="100px"><b>NPM</b></td>' +
                        '<td width="14px">:</td>' +
                        '<td>{no_identitas}</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td><b>Nama</b></td><td>:</td>' +
                        '<td>{nama_lengkap}</td>' +
                    '</tr>' + 
                    '<tr>' +
                        '<td vAlign="top"><b>Email</b></td><td>:</td>' +
                        '<td><div style="max-height:100px;overflow:auto;padding:1px">{user_email}</div></td>' +
                    '</tr>' + 
                    '<tr>' +
                        '<td><b>No. Telepon</b></td><td>:</td>' +
                        '<td>{no_telp}</td>' +
                    '</tr>' +
                    '<tr>' +
                        '<td><b>Alamat Web</b></td><td>:</td>' +
                        '<td>{user_web}</td></tr>' +
                    '<tr>' +
                        '<td vAlign="top"><b>Bio</b></td><td vAlign="top">:</td>' +
                        '<td><div style="max-height:100px;overflow:auto;padding:1px">{biodata}</div></td>' +
                    '</tr>' + 
                '</table>'
        },*/
        
            
    }]
});