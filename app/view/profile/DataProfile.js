Ext.define('DailyActivitiesScheduling.view.profile.DataProfile', {
    extend: 'Ext.tab.Panel',
    xtype: 'myprofile',
    id: 'data-profile',
    shadow: true,
    cls: 'demo-solid-background',

    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Ext.Menu',
        'DailyActivitiesScheduling.view.profile.ProfileController'
    ],

    controller: 'profile',

    tabBar: {
        layout: {
            pack: 'center'
        }
    },
    activeTab: 1,
    defaults: {
        scrollable: true
    },

    items: [
        {
            title: 'Info Pengembang',
            //layout: 'fit',
            xtype: 'basic-info',
            /*items: [
                {
                  xtype: 'pengembang',
                }
            ],*/
        },
        {
            title: 'Form Registrasi',
            layout: 'fit',
            items: [
                {
                  /*xtype: 'basic-info',*/
                  xtype: 'pengembang',
                },
            ],
        },
        /*{
            title: 'Read Data',
            layout: 'fit',
            items: [
                {
                  xtype: 'readData',
                },
            ],
        },*/
    ]

});