/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('DailyActivitiesScheduling.view.profile.MyInfo', {
    extend: 'Ext.form.Panel',
    xtype: 'basic-info',
    id: 'basic-info',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio',
        'Ext.Menu'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'basicform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1-myinfo',
            title: 'Informasi Akun',
            instructions: '<center><i>Mohon diisi dengan teliti</i></center>',
            defaults: {
                labelWidth: '35%',
                margin: 10,
            },
            items: [
                {
                    xtype: 'textfield',
                    id: 'myName',
                    name: 'name',
                    label: '<b>Nama Lengkap</b>',
                    placeHolder: 'nama lengkap',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    id: 'myNPM',
                    name: 'npm',
                    label: '<b>NPM/No. Identitas</b>',
                    placeHolder: '183510000',
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    id: 'myEmail',
                    name: 'email',
                    label: '<b>Alamat Email</b>',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    id: 'myPhone',
                    name: 'phone',
                    label: '<b>Phone</b>',
                    placeHolder: '08123456789',
                    clearIcon: true
                },
                {
                    label: '<b>Unggah Audio</b>',
                    html: 'Rekam Pengucapan: ' + 
                        '<div id="controls">' +
                            '<button id="recordCordova">Mulai Merekam Pengucapan</button>' +
                        '</div><br>' + 
                        '<div id="recordInfo"></div><br>'
                },
                {
                    xtype: 'urlfield',
                    id: 'mySite',
                    name: 'site',
                    label: '<b>Alamat Web (Opsional)</b>',
                    placeHolder: 'http://sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'urlfield',
                    id: 'myCover',
                    name: 'cover',
                    label: '<b>Cover Image (Harus berupa alamat link!)</b>',
                    placeHolder: 'https://my-image.jpg',
                    clearIcon: true
                },
                /*{ //Tanggal Lahir
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: '<b>Tanggal Lahir</b>',
                    value: new Date(),
                    picker: {
                        yearFrom: 1960
                    } 
                },*/
                /*{ //Jenis Kelamin
                    xtype: 'fieldset',
                    id: 'fieldset_gender',
                    title: 'Jenis Kelamin',
                    platformConfig: {
                        '!desktop': {
                            defaults: {
                                bodyAlign: 'end'
                            }
                        }
                    },
                    defaults: {
                        xtype: 'radiofield',
                        labelWidth: '35%',
                    },
                    items: [
                        {
                            name: 'gender',
                            value: 'male',
                            label: 'Laki-Laki'
                        },
                        {
                            name: 'gender',
                            value: 'female',
                            label: 'Perempuan'
                        },
                    ]
                },*/
                {
                    xtype: 'textareafield',
                    id: 'myBio',
                    name: 'bio',
                    label: '<b>Biodata</b>',
                }
            ]
        },
        /*{
            xtype: 'passwordfield',
            revealable: true,
            name : 'password',
            label: 'Password Baru',
            clearIcon: true
        },
        {
            xtype: 'passwordfield',
            revealable: true,
            name : 'password',
            label: 'Konformasi Password',
            clearIcon: true
        },*/
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Simpan Perubahan',
                    iconCls: 'x-fa fa-save',
                    ui: 'confirm',
                    /*handler: function(){
                        Ext.Msg.confirm('Simpan Data', '<center>Apakah data yang dimasukkan sudah benar?</center>', Ext.emptyFn);
                        //Ext.getCmp('basicform').reset();
                        //var selection = Ext.getCmp('basic-info').getValue();
                        //console.log();
                    }*/
                    handler: 'onSimpanPerubahan'
                },
                {
                    text: 'Tambah Data',
                    ui: 'action',
                    handler: 'onTambahData'
                },
                /*{
                    text: 'Opsi Lainnya',
                            handler: function() {
                        Ext.Viewport.toggleMenu('left');
                    }
                }*/
            ]
        }
    ],

    initialize: function() {
        Ext.Viewport.setMenu(this.getMenuCfg('left'), {
            side: 'left',
            reveal: true
        });
    },

    doDestroy: function() {
        Ext.Viewport.removeMenu('left');
        this.callParent();
    },

    getMenuCfg: function(side) {
        return {
            items: [{
                text: 'Home',
                iconCls: 'x-fa fa-home',
                scope: this,
                handler: function() {
                    var mainView = Ext.getCmp('app-main');
                    mainView.setActiveItem(0);
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                text: 'Input List',
                iconCls: 'x-fa fa-list',
                scope: this,
                handler: function() {
                    var mainView = Ext.getCmp('app-main');
                    mainView.setActiveItem(1);
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                text: 'Profil',
                iconCls: 'x-fa fa-users',
                scope: this,
                handler: function() {
                    Ext.getCmp('data-profile').setActiveItem(1);
                    Ext.Viewport.hideMenu(side);
                }
            }, {
                text: 'About',
                iconCls: 'x-fa fa-info-circle',
                scope: this,
                handler: function() {
                    var mainView = Ext.getCmp('app-main');
                    mainView.setActiveItem(3);
                    Ext.Viewport.hideMenu(side);
                }
            }, { xtype: 'spacer'}, 
            {
                text: 'Batal',
                iconCls: 'x-fa fa-times',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }, /*{
                xtype: 'button',
                text: 'Star',
                iconCls: 'x-fa fa-star',
                scope: this,
                handler: function() {
                    Ext.Viewport.hideMenu(side);
                }
            }*/
            ]
        };
    }

});