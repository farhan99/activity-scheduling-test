Ext.define('DailyActivitiesScheduling.view.profile.ReadData', {
    extend: 'Ext.grid.Grid',
    xtype: 'readData',

    requires: [
        //'DailyActivitiesScheduling.store.Individuals'
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    title: 'Individuals',

    store: {
        type: 'individuals'
    },

    columns: [
        { text: 'Nama Lengkap',  dataIndex: 'nama_lengkap', width: 100, editable: true },
        { text: 'No Identitas',  dataIndex: 'no_identitas', width: 100, editable: true },
        { text: 'Email',  dataIndex: 'user_email', width: 100, editable: true },
        { text: 'No. Telepon',  dataIndex: 'no_telp', width: 100, editable: true },
        { text: 'Gambar',  dataIndex: 'gambar', width: 100 },
        { text: 'Alamat Web', dataIndex: 'user_web', width: 230 },
        { text: 'Biodata', dataIndex: 'biodata', width: 150 }
    ],

    /*columns: [
        { text: 'Nama Lengkap',  dataIndex: 'nama_lengkap', width: 100 },
        { text: 'No Identitas',  dataIndex: 'no_identitas', width: 100 },
        { text: 'Email',  dataIndex: 'user_email', width: 100 },
        { text: 'No. Telepon',  dataIndex: 'no_telp', width: 100 },
        { text: 'Gambar',  dataIndex: 'gambar', width: 100 },
        { text: 'Alamat Web', dataIndex: 'user_web', width: 230 },
        { text: 'Biodata', dataIndex: 'biodata', width: 150 }
    ],*/

    listeners: {
        select: 'onItemSelected'
    }
});
