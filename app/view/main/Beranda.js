/**
 * Demonstrates how to use an Ext.Carousel in vertical and horizontal configurations
 */
Ext.define('DailyActivitiesScheduling.view.main.Beranda', {
    extend: 'Ext.Container',
	xtype: 'home-carousel',
	
    requires: [
        'Ext.carousel.Carousel'
    ],

    cls: 'cards',
    shadow: true,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
		id: 'carouselTop',
        flex: 1,
        items: [
		{
            html: '<p>You can also tap on either side of the indicators.</p>',
            style:
              'background-image: url("https://images.unsplash.com/photo-1541963463532-d68292c34b19?ixlib=rb-1.2.1&w=1000&q=80"); background-size: 100%;',
  
            cls: 'card',
        },
        
		{
            //Carousel 2 - Card#3
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
            style: 'background-color: #a9dba2;',
            defaults: {
              xtype: 'button',
              style: 'margin: 0.5em; background-color: #312836;',
            },
            layout: {
              type: 'vbox',
            },
            items: [
              {
                text: 'Ke Input List',
                ui: 'action',
                handler: function () {
                  var mainView = Ext.getCmp('formlist');
                  mainView.setActiveItem(1);
                },
              },
              {
                text: 'Ke Profile',
                ui: 'action',
                handler: function () {
                  var mainView = Ext.getCmp('carouselTop');
                  mainView.setActiveItem(1);
                },
              },
              {
                text: 'Ke About',
                ui: 'action',
                handler: function () {
                  var mainView = Ext.getCmp('app-pengembang');
                  mainView.setActiveItem(2);
                },
              },
            ],
        },
		
        {
            html: 'Card #3',
            cls: 'card'
        }]
    },/*{
        xtype: 'carousel',
        ui: 'light',
        direction: 'vertical',
        items: [{
            html: '<p>Carousels can also be vertical <em>(swipe up)…</p>',
            cls: 'card dark'
        },
        {
            html: 'And can also use <code>ui:light</code>.',
            cls: 'card dark'
        },
        {
            html: 'Card #3',
            cls: 'card dark'
        }]
    }*/
	{
		xtype: 'mainlist'
	}
	
	
	]
});