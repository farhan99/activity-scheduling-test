/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('DailyActivitiesScheduling.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
	id:'app-main',
    requires: [
        'Ext.MessageBox',

        'DailyActivitiesScheduling.view.main.MainController',
        'DailyActivitiesScheduling.view.main.MainModel',
        'DailyActivitiesScheduling.view.main.List',
		'DailyActivitiesScheduling.view.IsiList.FormList',
		'DailyActivitiesScheduling.view.profile.Pengembang',
		'DailyActivitiesScheduling.view.profile.DataProfile',
		'DailyActivitiesScheduling.view.profile.MyInfo',
		'DailyActivitiesScheduling.view.main.Beranda',
        'DailyActivitiesScheduling.store.Personnel'
    ],

    controller: 'main',
    viewModel: 'main',

    store: {
        type: 'individuals'
        //type: 'personnel'
    },

   defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
				/*xtype: 'home-carousel'*/
                xtype: 'myprofile'
            }]
        },{
            title: 'Input List',
            iconCls: 'x-fa fa-list',
			layout: 'fit',
            items: [{
                xtype: 'formlist'
            }]
        },{
            title: 'Profile',
            iconCls: 'x-fa fa-users',
			layout: 'fit',
            items: [{
                /*xtype: 'myprofile'*/
                xtype: 'home-carousel'
            }]
        },{
            title: 'About',
            iconCls: 'x-fa fa-info-circle',
            layout: 'fit',
            bind: {
                html: '{loremIpsum}'
            }
        }
    ]
});
