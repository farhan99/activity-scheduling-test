Ext.define('DailyActivitiesScheduling.view.main.List', {
    extend: 'Ext.grid.Grid',
	xtype: 'mainlist',
	id: 'mainlist',
    requires: [
        'DailyActivitiesScheduling.store.Personnel',

    ],
	
    title: 'JADWAL ANDA',
	shadow: true,
	cls: 'demo-solid-background',
	
	store: {
		type: 'personnel'
	},
	columns: [
		{ text: '<b>HARI</b>',  dataIndex: 'hari', width: 100 },
		{ text: '<b>AKTIVITAS</b>', dataIndex: 'aktivitas', width: 230 },
		{ text: '<b>WAKTU</b>', dataIndex: 'waktu', width: 150 },
		{ text: '<b>KETERANGAN</b>', dataIndex: 'ket', width: 150 }		
	],
	listeners: {
		select: 'onItemSelected'
	}
    
});
