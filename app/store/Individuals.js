Ext.define('DailyActivitiesScheduling.store.Individuals', {
    extend: 'Ext.data.Store',
    
    alias: 'store.individuals',
    storeId: 'individuals',
    autoLoad: true,
    autoSync: true,

    fields: ['nama_lengkap', 'no_identitas', 'user_email', 'no_telp', 'gambar', 'user_web', 'biodata'],
  
    /*data: {
      items: [
        {
          nama: 'Farhan',
          npm: '183510938',
          email: 'farhan99@student.uir.ac.id',
          telepon: '555-111-1111',
          gambar: 'https://image.freepik.com/free-vector/businessman-profile-cartoon_18591-58481.jpg',
          situs: 'https://www.farhan.com/',
          bagian: 'Profile',
          bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate eros congue, lobortis libero eu, faucibus tellus. In hac habitasse platea dictumst. Nunc venenatis tempor blandit. Sed in erat nunc. Mauris dui libero, pulvinar ac rhoncus sed, pellentesque vel arcu. Aliquam non ornare erat, et tincidunt nibh. Sed congue non nisi id maximus. Quisque ut condimentum nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ante neque, scelerisque quis tincidunt vitae, condimentum eget velit. Morbi fringilla augue rutrum, imperdiet enim euismod, mattis est. Praesent congue in mauris aliquet blandit. Nulla ac felis ut est dapibus porttitor. Suspendisse ullamcorper varius odio a commodo.',
        },
        {
          nama: 'Haekal Ramadhan',
          npm: '183510911',
          email: 'worf.moghsson@enterprise.com',
          telepon: '555-222-2222',
          situs: 'https://www.sencha.com/',
          bagian: 'Input List',
          gambar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSe24kvLyT90lF_F7Rqto-rhLtarBIjYclmqA&usqp=CAU',
          bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate eros congue, lobortis libero eu, faucibus tellus. In hac habitasse platea dictumst. Nunc venenatis tempor blandit. Sed in erat nunc. Mauris dui libero, pulvinar ac rhoncus sed, pellentesque vel arcu. Aliquam non ornare erat, et tincidunt nibh. Sed congue non nisi id maximus. Quisque ut condimentum nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ante neque, scelerisque quis tincidunt vitae, condimentum eget velit. Morbi fringilla augue rutrum, imperdiet enim euismod, mattis est. Praesent congue in mauris aliquet blandit. Nulla ac felis ut est dapibus porttitor. Suspendisse ullamcorper varius odio a commodo.',
        },
        {
            nama: 'Dicky',
            npm: '33333333w',
            email: 'deanna.troi@enterprise.com',
            telepon: '555-333-3333',
            gambar: 'https://image.freepik.com/free-vector/man-profile-cartoon_18591-58483.jpg',
            situs: 'https://www.sencha.com/',
            bagian: 'Home',
            bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate eros congue, lobortis libero eu, faucibus tellus. In hac habitasse platea dictumst. Nunc venenatis tempor blandit. Sed in erat nunc. Mauris dui libero, pulvinar ac rhoncus sed, pellentesque vel arcu. Aliquam non ornare erat, et tincidunt nibh. Sed congue non nisi id maximus. Quisque ut condimentum nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ante neque, scelerisque quis tincidunt vitae, condimentum eget velit. Morbi fringilla augue rutrum, imperdiet enim euismod, mattis est. Praesent congue in mauris aliquet blandit. Nulla ac felis ut est dapibus porttitor. Suspendisse ullamcorper varius odio a commodo.',
        },
        {
            nama: 'Bayu',
            npm: '44444444',
            email: 'mr.data@enterprise.com',
            telepon: '555-444-4444',
            gambar: 'https://image.freepik.com/free-vector/businessman-profile-cartoon_18591-58479.jpg',
            situs: 'https://www.sencha.com/',
          bagian: 'About',
          bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vulputate eros congue, lobortis libero eu, faucibus tellus. In hac habitasse platea dictumst. Nunc venenatis tempor blandit. Sed in erat nunc. Mauris dui libero, pulvinar ac rhoncus sed, pellentesque vel arcu. Aliquam non ornare erat, et tincidunt nibh. Sed congue non nisi id maximus. Quisque ut condimentum nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer ante neque, scelerisque quis tincidunt vitae, condimentum eget velit. Morbi fringilla augue rutrum, imperdiet enim euismod, mattis est. Praesent congue in mauris aliquet blandit. Nulla ac felis ut est dapibus porttitor. Suspendisse ullamcorper varius odio a commodo.',
        },
      ],
    },*/
    
    proxy: {
      type: 'jsonp',
      api: {
        read   : DailyActivitiesScheduling.util.Globals.getPhppath() + 'DAS_php/readProfile.php',
        create : 'DAS_php/addProfile.php',
        update : 'DAS_php/updateProfile.php',
        destroy: 'DAS_php/destroyProfile.php'
      },
      reader: {
        type: 'json',
        rootProperty: 'items',
      },
    },

    //Listeners
    listeners: {
        beforeload: function (store, operation, e0pts) {
            this.getProxy().setExtraParams({
                //user_id: localStorage.getItem('user_id'), //when user login
                user_id: -1,
            }); 
        }
    }

    /*listeners: {
        beforeload: function(store, operation, e0pts){
            this.getProxy().setExtraParams({
                user_id: localStorage.getItem('user_id'),
                role: localStorage.getItem('role')
            });
        }
    }*/
  });
  