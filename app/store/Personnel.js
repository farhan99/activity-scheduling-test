Ext.define('DailyActivitiesScheduling.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',

    fields: [
        'hari', 'aktivitas', 'waktu','ket'
    ],

    data: { items: [
        { hari: 'Senin', 	  aktivitas: "TUGAS DPM", 			waktu: "23.59", ket: "NO" },
        { hari: '',     	  aktivitas: "TUGAS GRAFIKA KOM",  	waktu: "23.59", ket: "NO" },
        { hari: '',   		  aktivitas: "TUGAS DPM KELOMPOK",  waktu: "23.59", ket: "NO" },
        { hari: 'Selasa',     aktivitas: "TUGAS DPM LAB",       waktu: "23.59", ket: "NO" }
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
